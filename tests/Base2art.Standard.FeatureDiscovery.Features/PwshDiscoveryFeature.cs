namespace Base2art.FeatureDiscovery.Features
{
    using System;
    using System.Linq;
    using CommonFeatures;
    using FluentAssertions;
    using Hashing;
    using Xunit;
    using Xunit.Abstractions;

    public class PwshDiscoveryFeature
    {
        public PwshDiscoveryFeature(ITestOutputHelper testOutputHelper) => this.testOutputHelper = testOutputHelper;

        private readonly ITestOutputHelper testOutputHelper;

        private const string WinMachine1 = "59beab3758ca2f5b24996d6c763f512f";
        private const string BuildWinMachine1 = "210aff9c-3c70-07d5-13b9-c52efb51e52f";
        private const string FedoraMachine1 = "60c9881949479c0abdc471d71dbaffc6";

        public static Guid[] KnownMachines => new[] {new Guid(WinMachine1), new Guid(FedoraMachine1), new Guid(BuildWinMachine1)};

        [Theory]
        [InlineData("powershell8", typeof(Powershell8Discovery), new string[] { })]
        [InlineData("powershell7", typeof(Powershell7Discovery), new string[] { })]
        [InlineData("powershell6", typeof(Powershell6Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("powershell5_1", typeof(Powershell5_1Discovery), new[] {WinMachine1})]
        [InlineData("powershell5", typeof(Powershell5Discovery), new[] {WinMachine1})]
        [InlineData("powershell4", typeof(Powershell4Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("powershell3", typeof(Powershell3Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("powershell2", typeof(Powershell2Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("powershell1", typeof(Powershell1Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net48", typeof(NetFramework480Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net472", typeof(NetFramework472Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net471", typeof(NetFramework471Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net47", typeof(NetFramework470Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net462", typeof(NetFramework462Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net461", typeof(NetFramework461Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net46", typeof(NetFramework460Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net452", typeof(NetFramework452Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net451", typeof(NetFramework451Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net45", typeof(NetFramework450Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net35", typeof(NetFramework35Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net30", typeof(NetFramework30Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net20", typeof(NetFramework20Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net48-sdk", typeof(NetFrameworkSdk480Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net472-sdk", typeof(NetFrameworkSdk472Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net471-sdk", typeof(NetFrameworkSdk471Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net47-sdk", typeof(NetFrameworkSdk470Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net462-sdk", typeof(NetFrameworkSdk462Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net461-sdk", typeof(NetFrameworkSdk461Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net46-sdk", typeof(NetFrameworkSdk460Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net452-sdk", typeof(NetFrameworkSdk452Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net451-sdk", typeof(NetFrameworkSdk451Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net45-sdk", typeof(NetFrameworkSdk450Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("net35-sdk", typeof(NetFrameworkSdk35Discovery), new[] {BuildWinMachine1})]
        [InlineData("net30-sdk", typeof(NetFrameworkSdk30Discovery), new[] {BuildWinMachine1})]
        [InlineData("net20-sdk", typeof(NetFrameworkSdk20Discovery), new[] {WinMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-1.0", typeof(DotNetCore10Discovery), new string[0])]
        [InlineData("dotnet-core-1.1", typeof(DotNetCore11Discovery), new string[0])]
        [InlineData("dotnet-core-2.0", typeof(DotNetCore20Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-2.1", typeof(DotNetCore21Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-2.2", typeof(DotNetCore22Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-3.0", typeof(DotNetCore30Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-3.1", typeof(DotNetCore31Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-5.0", typeof(DotNetCore50Discovery), new string[0])]
        [InlineData("dotnet-core-5.1", typeof(DotNetCore51Discovery), new string[0])]
        [InlineData("dotnet-core-sdk-1.0", typeof(DotNetCoreSdk10Discovery), new string[0])]
        [InlineData("dotnet-core-sdk-1.1", typeof(DotNetCoreSdk11Discovery), new string[0])]
        [InlineData("dotnet-core-sdk-2.0", typeof(DotNetCoreSdk20Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-sdk-2.1", typeof(DotNetCoreSdk21Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-sdk-2.2", typeof(DotNetCoreSdk22Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-sdk-3.0", typeof(DotNetCoreSdk30Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-sdk-3.1", typeof(DotNetCoreSdk31Discovery), new[] {WinMachine1, FedoraMachine1, BuildWinMachine1})]
        [InlineData("dotnet-core-sdk-5.0", typeof(DotNetCoreSdk50Discovery), new string[0])]
        [InlineData("dotnet-core-sdk-5.1", typeof(DotNetCoreSdk51Discovery), new string[0])]
        [InlineData("yarn", typeof(YarnDiscovery), new[] {FedoraMachine1})]
        [InlineData("maven", typeof(MavenDiscovery), new[] {FedoraMachine1})]
        public async void ShouldLoad(string featureName, Type discoveryType, string[] machineNames)
        {
            var item = Activator.CreateInstance(discoveryType).As<IFeatureDiscovery>();
            var feature = await item.HasFeature();

            var machineHash = Environment.MachineName.ToUpperInvariant().HashAsGuid();
            this.testOutputHelper.WriteLine(machineHash.ToString());
            KnownMachines.Should().ContainSingle(x => x == machineHash, machineHash.ToString("N"));

            if (machineNames.Any(x => new Guid(x) == machineHash))
            {
                feature.Should().BeTrue();
                item.FeatureName.Should().Be(featureName);
            }
            else
            {
                feature.Should().BeFalse();
                item.FeatureName.Should().Be(featureName, "UNKNOWN MACHINE");
            }
        }

        [Fact]
        public async void ShouldDiscover()
        {
            var provider = new FeatureDiscoveryProvider();
            var items = await provider.CreateFeatures();
            items.Should().HaveCount(55);
        }
    }
}