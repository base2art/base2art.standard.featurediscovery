namespace Base2art.FeatureDiscovery
{
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Diagnostic;

    public class RelativeFolderExistsDiscovery : IFeatureDiscovery
    {
        private readonly HashSet<string> entryNames;
        private readonly string[] exeNames;
        private readonly string featureName;
        private readonly string[] relativePathParts;

        protected RelativeFolderExistsDiscovery(string featureName,
                                                string[] exeNames,
                                                string[] relativePathParts,
                                                string[] entryNames)
        {
            this.featureName = featureName;
            this.exeNames = exeNames ?? new string[0];
            this.relativePathParts = relativePathParts ?? new string[0];
            this.entryNames = new HashSet<string>(entryNames ?? new string[0]);
        }

        string IFeatureDiscovery.FeatureName => this.featureName;

        async Task<bool> IFeatureDiscovery.HasFeature()
        {
            var info = await Environments.FindFileInPath(this.exeNames);

            if (info?.Exists == true)
            {
                var parts = new List<string>();
                parts.Add(info.Directory.FullName);
                parts.AddRange(this.relativePathParts);
                var pathOf = Path.Combine(parts.ToArray());
                if (Directory.Exists(pathOf))
                {
                    var dir = new DirectoryInfo(pathOf);
                    foreach (var fileSystemInfo in dir.GetFileSystemInfos())
                    {
                        if (this.entryNames.Contains(fileSystemInfo.Name))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}