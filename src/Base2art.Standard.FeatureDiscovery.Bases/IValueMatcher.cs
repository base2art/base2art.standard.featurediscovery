namespace Base2art.FeatureDiscovery
{
    public interface IValueMatcher
    {
        bool IsMatch(string value, string searchString);
    }
}