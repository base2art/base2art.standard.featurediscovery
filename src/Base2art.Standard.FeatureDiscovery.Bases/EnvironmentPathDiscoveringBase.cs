namespace Base2art.FeatureDiscovery
{
    using System.IO;
    using System.Threading.Tasks;
    using Diagnostic;

    public class EnvironmentPathDiscoveringBase : IFeatureDiscovery
    {
        private readonly string[] exeNames;
        private readonly string featureName;

        protected EnvironmentPathDiscoveringBase(string featureName, params string[] exeNames)
        {
            this.featureName = featureName;
            this.exeNames = exeNames ?? new string[0];
        }

        string IFeatureDiscovery.FeatureName => this.featureName;

        async Task<bool> IFeatureDiscovery.HasFeature()
        {
            var info = await Environments.FindFileInPath(this.exeNames);

            if (info == null)
            {
                return false;
            }

            return this.IsMatch(info);
        }

        protected virtual bool IsMatch(FileInfo info) => true;
    }
}