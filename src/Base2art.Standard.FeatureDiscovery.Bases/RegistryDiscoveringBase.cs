namespace Base2art.FeatureDiscovery
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Diagnostic;

    public abstract class RegistryDiscoveringBase : IFeatureDiscovery
    {
        private readonly string expected;
        private readonly IProcessBuilderFactory factory;
        private readonly string featureName;
        private readonly string key;
        private readonly string keyType;
        private readonly string location;
        private readonly IValueMatcher matcher;

        protected RegistryDiscoveringBase(
            string featureName,
            string location,
            string keyType,
            string key,
            IValueMatcher matcher,
            string expected)
        {
            this.featureName = featureName;
            this.location = location;
            this.keyType = keyType;
            this.key = key;
            this.matcher = matcher;
            this.expected = expected;
            this.factory = new ProcessBuilderFactory();
        }

        string IFeatureDiscovery.FeatureName => this.featureName;

        async Task<bool> IFeatureDiscovery.HasFeature()
        {
            var folders = new[] {Environment.SpecialFolder.System, Environment.SpecialFolder.SystemX86};
            foreach (var folder in folders)
            {
                if (await this.IsFound(folder))
                {
                    return true;
                }
            }

            return false;
        }

        private async Task<bool> IsFound(Environment.SpecialFolder folder)
        {
            var path = Environment.GetFolderPath(folder);
            var regFile = Path.Combine(path, "reg.exe");

            if (File.Exists(regFile))
            {
                var output = "";
                using (var sr = new StringWriter())
                {
                    await this.factory
                              .Create()
                              .WithExecutable(regFile)
                              .WithOutputWriter(sr)
                              .WithArguments(new[]
                                             {
                                                 "QUERY",
                                                 this.location,
                                                 "/v",
                                                 this.key
                                             })
                              .Execute();

                    output = sr.GetStringBuilder().ToString();
                }

                if (output.Contains(this.keyType))
                {
                    var parts = output.Split(new[] {this.keyType}, StringSplitOptions.RemoveEmptyEntries);
                    var last = parts.Last().Trim();
                    if (this.IsMatch(last))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsMatch(string last) => this.matcher.IsMatch(last, this.expected);
    }
}