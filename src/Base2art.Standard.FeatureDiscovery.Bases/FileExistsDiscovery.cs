namespace Base2art.FeatureDiscovery
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    public class FileExistsDiscovery : IFeatureDiscovery
    {
        private readonly string featureName;
        private readonly string[] relativePathParts;
        private readonly Environment.SpecialFolder root;

        protected FileExistsDiscovery(string featureName,
                                      Environment.SpecialFolder root,
                                      params string[] relativePathParts)
        {
            this.featureName = featureName;
            this.root = root;
            this.relativePathParts = relativePathParts;
        }

        string IFeatureDiscovery.FeatureName => this.featureName;

        Task<bool> IFeatureDiscovery.HasFeature()
        {
            var parts = new List<string> {Environment.GetFolderPath(this.root)};
            parts.AddRange(this.relativePathParts);

            var path = Path.Combine(parts.ToArray());
            return Task.FromResult(File.Exists(path));
        }
    }
}