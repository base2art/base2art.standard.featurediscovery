namespace Base2art.FeatureDiscovery
{
    using System;
    using System.ComponentModel;
    using System.Linq;

    public static class ValueMatchers
    {
        public static IValueMatcher CommaStringSplitsContains => new CommaStringSplitsContainsMatcher(StringComparison.Ordinal);

        public static IValueMatcher CommaStringSplitsContainsIgnoreCase => new CommaStringSplitsContainsMatcher(StringComparison.OrdinalIgnoreCase);

        public static IValueMatcher IntGreaterThanOrEqual => new IntCompare((x, y) => x >= y);

        public static IValueMatcher IntEqual => new IntCompare((x, y) => x == y);

        private class CommaStringSplitsContainsMatcher : IValueMatcher
        {
            private readonly StringComparison stringComparison;

            public CommaStringSplitsContainsMatcher(StringComparison stringComparison) => this.stringComparison = stringComparison;

            public bool IsMatch(string value, string searchString)
            {
                value = value ?? string.Empty;

                var parts = value.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);

                parts = parts.Select(x => x.Trim()).ToArray();

                return parts.Any(x => string.Equals(x, searchString, this.stringComparison));
            }
        }

        private class IntCompare : IValueMatcher
        {
            private readonly Func<int, int, bool> func;

            public IntCompare(Func<int, int, bool> func) => this.func = func;

            public bool IsMatch(string value, string searchString)
            {
                var converter = new Int32Converter();
                try
                {
                    var regValue = (int) converter.ConvertFromString(value);
                    var searchStringInt = (int) converter.ConvertFromString(searchString);

                    return this.func(regValue, searchStringInt);
                }
                catch (ArgumentException)
                {
                    return false;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
        }
    }
}