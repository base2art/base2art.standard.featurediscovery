namespace Base2art.FeatureDiscovery
{
    using System.Threading.Tasks;

    public interface IFeatureDiscovery
    {
        string FeatureName { get; }

        Task<bool> HasFeature();
    }
}