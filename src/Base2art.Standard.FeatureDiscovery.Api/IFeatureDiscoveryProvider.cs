namespace Base2art.FeatureDiscovery
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFeatureDiscoveryProvider
    {
        Task<IEnumerable<IFeatureDiscovery>> CreateFeatures();
    }
}