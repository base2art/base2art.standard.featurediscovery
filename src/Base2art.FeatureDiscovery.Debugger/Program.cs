﻿namespace Base2art.FeatureDiscovery.Debugger
{
    using System;
    using System.Threading.Tasks;
    using CommonFeatures;

    internal class Program
    {
        private static async Task<int> Main(string[] args)
        {
            var provider = new FeatureDiscoveryProvider();
            var features = await provider.CreateFeatures();

            foreach (var feature in features)
            {
                if (await feature.HasFeature())
                {
                    Console.WriteLine(feature.FeatureName);
                }
            }

            return 0;
        }
    }
}