namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore20Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore20Discovery() : base(2, 0)
        {
        }
    }
}