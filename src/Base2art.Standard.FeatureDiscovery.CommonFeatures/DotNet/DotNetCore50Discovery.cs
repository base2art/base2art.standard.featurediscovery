namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore50Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore50Discovery() : base(5, 0)
        {
        }
    }
}