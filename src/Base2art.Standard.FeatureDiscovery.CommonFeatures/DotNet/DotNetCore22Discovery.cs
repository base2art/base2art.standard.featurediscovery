namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore22Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore22Discovery() : base(2, 2)
        {
        }
    }
}