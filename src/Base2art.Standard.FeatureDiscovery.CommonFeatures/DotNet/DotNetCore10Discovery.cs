namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore10Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore10Discovery() : base(1, 0)
        {
        }
    }
}