namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System.Linq;

    public abstract class DotNetCoreDiscoveryBase : RelativeFolderExistsDiscovery
    {
        protected DotNetCoreDiscoveryBase(int major, int minor) : base(
                                                                       $"dotnet-core-{major}.{minor}",
                                                                       new[] {"dotnet", "dotnet.exe"},
                                                                       new[] {@"shared", "Microsoft.NETCore.App"},
                                                                       Enumerable.Range(minor, 100)
                                                                                 .SelectMany(minorInner => Enumerable
                                                                                                           .Range(0, 2000)
                                                                                                           .Select(b => $"{major}.{minorInner}.{b}"))
                                                                                 .ToArray())
        {
        }
    }
}