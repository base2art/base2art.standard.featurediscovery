namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore30Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore30Discovery() : base(3, 0)
        {
        }
    }
}