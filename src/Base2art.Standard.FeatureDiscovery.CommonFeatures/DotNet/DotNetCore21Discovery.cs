namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore21Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore21Discovery() : base(2, 1)
        {
        }
    }
}