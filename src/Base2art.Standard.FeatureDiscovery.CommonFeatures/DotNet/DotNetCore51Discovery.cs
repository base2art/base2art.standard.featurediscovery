namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore51Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore51Discovery() : base(5, 1)
        {
        }
    }
}