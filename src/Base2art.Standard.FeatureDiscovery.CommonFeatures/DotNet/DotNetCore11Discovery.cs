namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore11Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore11Discovery() : base(1, 1)
        {
        }
    }
}