namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCore31Discovery : DotNetCoreDiscoveryBase
    {
        public DotNetCore31Discovery() : base(3, 1)
        {
        }
    }
}