namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class Powershell5Discovery : RegistryDiscoveringBase
    {
        public Powershell5Discovery() : base(
                                             "powershell5",
                                             @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine",
                                             "REG_SZ",
                                             "PSCompatibleVersion",
                                             ValueMatchers.CommaStringSplitsContains,
                                             "5.0")
        {
        }
    }
}