namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class Powershell1Discovery : RegistryDiscoveringBase
    {
        public Powershell1Discovery() : base(
                                             "powershell1",
                                             @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine",
                                             "REG_SZ",
                                             "PSCompatibleVersion",
                                             ValueMatchers.CommaStringSplitsContains,
                                             "1.0")
        {
        }
    }
}