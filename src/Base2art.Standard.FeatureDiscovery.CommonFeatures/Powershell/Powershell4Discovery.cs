namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class Powershell4Discovery : RegistryDiscoveringBase
    {
        public Powershell4Discovery() : base(
                                             "powershell4",
                                             @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine",
                                             "REG_SZ",
                                             "PSCompatibleVersion",
                                             ValueMatchers.CommaStringSplitsContains,
                                             "4.0")
        {
        }
    }
}