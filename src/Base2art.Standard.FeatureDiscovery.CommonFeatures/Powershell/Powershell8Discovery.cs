namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System.IO;
    using System.Linq;

    public class Powershell8Discovery : EnvironmentPathDiscoveringBase
    {
        public Powershell8Discovery() : base("powershell8", "pwsh", "pwsh.exe")
        {
        }

        protected override bool IsMatch(FileInfo info)
        {
            var powershellInstallDir = info.Directory.Parent;

            return powershellInstallDir.GetDirectories().Select(x => x.Name).Any(x => x == "8" || x.StartsWith("8."));
        }
    }
}