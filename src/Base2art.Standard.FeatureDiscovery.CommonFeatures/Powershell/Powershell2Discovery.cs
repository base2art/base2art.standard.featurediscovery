namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class Powershell2Discovery : RegistryDiscoveringBase
    {
        public Powershell2Discovery() : base(
                                             "powershell2",
                                             @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine",
                                             "REG_SZ",
                                             "PSCompatibleVersion",
                                             ValueMatchers.CommaStringSplitsContains,
                                             "2.0")
        {
        }
    }
}