namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class Powershell3Discovery : RegistryDiscoveringBase
    {
        public Powershell3Discovery() : base(
                                             "powershell3",
                                             @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine",
                                             "REG_SZ",
                                             "PSCompatibleVersion",
                                             ValueMatchers.CommaStringSplitsContains,
                                             "3.0")
        {
        }
    }
}