namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System.IO;
    using System.Linq;

    public class Powershell7Discovery : EnvironmentPathDiscoveringBase
    {
        public Powershell7Discovery() : base("powershell7", "pwsh", "pwsh.exe")
        {
        }

        protected override bool IsMatch(FileInfo info)
        {
            var powershellInstallDir = info.Directory.Parent;

            return powershellInstallDir.GetDirectories().Select(x => x.Name).Any(x => x == "7" || x.StartsWith("7."));
        }
    }
}