namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class Powershell5_1Discovery : RegistryDiscoveringBase
    {
        public Powershell5_1Discovery() : base(
                                               "powershell5_1",
                                               @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine",
                                               "REG_SZ",
                                               "PSCompatibleVersion",
                                               ValueMatchers.CommaStringSplitsContains,
                                               "5.1")
        {
        }
    }
}