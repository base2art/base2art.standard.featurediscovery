namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System.IO;
    using System.Linq;

    public class Powershell6Discovery : EnvironmentPathDiscoveringBase
    {
        public Powershell6Discovery() : base("powershell6", "pwsh", "pwsh.exe")
        {
        }

        protected override bool IsMatch(FileInfo info)
        {
            var powershellInstallDir = info.Directory.Parent;

            return powershellInstallDir.GetDirectories().Select(x => x.Name).Any(x => x == "6" || x.StartsWith("6."));
        }
    }
}