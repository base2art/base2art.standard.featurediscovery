namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class YarnDiscovery : EnvironmentPathDiscoveringBase
    {
        public YarnDiscovery() : base("yarn", "yarn", "yarn.exe")
        {
        }
    }
}