namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class MavenDiscovery : EnvironmentPathDiscoveringBase
    {
        public MavenDiscovery() : base("maven", "mvn", "mvn.exe")
        {
        }
    }
}