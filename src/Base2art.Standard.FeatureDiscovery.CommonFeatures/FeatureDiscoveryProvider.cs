namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    public class FeatureDiscoveryProvider : IFeatureDiscoveryProvider
    {
        public Task<IEnumerable<IFeatureDiscovery>> CreateFeatures()
        {
            var @interface = typeof(IFeatureDiscovery);

            var items = Assembly.GetExecutingAssembly()
                                .GetExportedTypes()
                                .Where(p => @interface.IsAssignableFrom(p))
                                .Where(x => x.IsClass && !x.IsAbstract && !x.IsInterface)
                                .Select(Activator.CreateInstance)
                                .Cast<IFeatureDiscovery>();
            return Task.FromResult(items);
        }
    }
}