namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework471Discovery : RegistryDiscoveringBase
    {
        public NetFramework471Discovery() : base(
                                                 "net471",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "461308")
        {
        }
    }
}