namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework462Discovery : RegistryDiscoveringBase
    {
        public NetFramework462Discovery() : base(
                                                 "net462",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "394802")
        {
        }
    }
}