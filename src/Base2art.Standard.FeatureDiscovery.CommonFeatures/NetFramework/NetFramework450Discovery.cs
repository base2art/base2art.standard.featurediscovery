namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework450Discovery : RegistryDiscoveringBase
    {
        public NetFramework450Discovery() : base(
                                                 "net45",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "378389")
        {
        }
    }
}