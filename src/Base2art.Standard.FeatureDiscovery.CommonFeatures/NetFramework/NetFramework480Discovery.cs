namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework480Discovery : RegistryDiscoveringBase
    {
        public NetFramework480Discovery() : base(
                                                 "net48",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "528040")
        {
        }
    }
}