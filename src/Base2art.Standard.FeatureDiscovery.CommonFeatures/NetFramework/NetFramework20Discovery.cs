namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework20Discovery : RegistryDiscoveringBase
    {
        public NetFramework20Discovery() : base(
                                                "net20",
                                                @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727\",
                                                "REG_DWORD",
                                                "Install",
                                                ValueMatchers.IntEqual,
                                                "1")
        {
        }
    }
}