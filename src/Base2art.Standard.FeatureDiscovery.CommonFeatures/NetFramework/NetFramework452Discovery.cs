namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework452Discovery : RegistryDiscoveringBase
    {
        public NetFramework452Discovery() : base(
                                                 "net452",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "379893")
        {
        }
    }
}