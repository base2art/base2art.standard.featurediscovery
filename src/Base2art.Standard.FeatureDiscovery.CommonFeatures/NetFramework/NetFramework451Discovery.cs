namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework451Discovery : RegistryDiscoveringBase
    {
        public NetFramework451Discovery() : base(
                                                 "net451",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "378675")
        {
        }
    }
}