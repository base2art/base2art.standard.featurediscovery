namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework30Discovery : RegistryDiscoveringBase
    {
        public NetFramework30Discovery() : base(
                                                "net30",
                                                @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0\",
                                                "REG_DWORD",
                                                "Install",
                                                ValueMatchers.IntEqual,
                                                "1")
        {
        }
    }
}