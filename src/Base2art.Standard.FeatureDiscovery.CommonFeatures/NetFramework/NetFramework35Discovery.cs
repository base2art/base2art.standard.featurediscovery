namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework35Discovery : RegistryDiscoveringBase
    {
        public NetFramework35Discovery() : base(
                                                "net35",
                                                @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5\",
                                                "REG_DWORD",
                                                "Install",
                                                ValueMatchers.IntEqual,
                                                "1")
        {
        }
    }
}