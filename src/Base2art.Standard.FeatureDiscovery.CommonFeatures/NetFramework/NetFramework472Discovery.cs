namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework472Discovery : RegistryDiscoveringBase
    {
        public NetFramework472Discovery() : base(
                                                 "net472",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "461808")
        {
        }
    }
}