namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework470Discovery : RegistryDiscoveringBase
    {
        public NetFramework470Discovery() : base(
                                                 "net47",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "460798")
        {
        }
    }
}