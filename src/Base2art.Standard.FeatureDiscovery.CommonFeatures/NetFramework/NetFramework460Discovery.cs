namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class NetFramework460Discovery : RegistryDiscoveringBase
    {
        public NetFramework460Discovery() : base(
                                                 "net46",
                                                 @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\",
                                                 "REG_DWORD",
                                                 "Release",
                                                 ValueMatchers.IntGreaterThanOrEqual,
                                                 "393295")
        {
        }
    }
}