namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk20Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk20Discovery() : base(2, 0)
        {
        }
    }
}