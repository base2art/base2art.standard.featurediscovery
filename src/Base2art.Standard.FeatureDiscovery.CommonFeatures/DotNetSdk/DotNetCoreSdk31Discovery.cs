namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk31Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk31Discovery() : base(3, 1)
        {
        }
    }
}