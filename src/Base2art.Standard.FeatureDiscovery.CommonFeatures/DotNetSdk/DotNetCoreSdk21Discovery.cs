namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk21Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk21Discovery() : base(2, 1)
        {
        }
    }
}