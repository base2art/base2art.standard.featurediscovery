namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk30Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk30Discovery() : base(3, 0)
        {
        }
    }
}