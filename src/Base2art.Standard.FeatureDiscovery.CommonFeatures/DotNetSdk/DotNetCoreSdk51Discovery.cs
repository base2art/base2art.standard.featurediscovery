namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk51Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk51Discovery() : base(5, 1)
        {
        }
    }
}