namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk22Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk22Discovery() : base(2, 2)
        {
        }
    }
}