namespace Base2art.FeatureDiscovery.CommonFeatures
{
    public class DotNetCoreSdk10Discovery : DotNetCoreSdkDiscoveryBase
    {
        public DotNetCoreSdk10Discovery() : base(1, 0)
        {
        }
    }
}