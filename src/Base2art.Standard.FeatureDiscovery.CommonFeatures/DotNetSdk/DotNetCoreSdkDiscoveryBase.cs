namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System.Linq;

    public abstract class DotNetCoreSdkDiscoveryBase : RelativeFolderExistsDiscovery
    {
        protected DotNetCoreSdkDiscoveryBase(int major, int minor) : base(
                                                                          $"dotnet-core-sdk-{major}.{minor}",
                                                                          new[] {"dotnet", "dotnet.exe"},
                                                                          new[] {"sdk"},
                                                                          Enumerable.Range(minor, 100)
                                                                                    .SelectMany(minorInner => Enumerable
                                                                                                              .Range(0, 2000)
                                                                                                              .Select(b =>
                                                                                                                          $"{major}.{minorInner}.{b}"))
                                                                                    .ToArray())
        {
        }
    }
}