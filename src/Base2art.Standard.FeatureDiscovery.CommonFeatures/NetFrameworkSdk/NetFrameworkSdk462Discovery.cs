namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk462Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk462Discovery() : base(
                                                    "net462-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.6.2",
                                                    "System.dll")

        {
        }
    }
}