namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk450Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk450Discovery() : base(
                                                    "net45-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.5",
                                                    "System.dll")

        {
        }
    }
}