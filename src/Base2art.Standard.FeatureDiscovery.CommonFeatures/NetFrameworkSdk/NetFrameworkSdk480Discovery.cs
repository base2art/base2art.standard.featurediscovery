namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk480Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk480Discovery() : base(
                                                    "net48-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.8",
                                                    "System.dll")

        {
        }
    }
}