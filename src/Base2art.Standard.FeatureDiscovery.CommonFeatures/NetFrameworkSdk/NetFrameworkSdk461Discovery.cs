namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk461Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk461Discovery() : base(
                                                    "net461-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.6.1",
                                                    "System.dll")

        {
        }
    }
}