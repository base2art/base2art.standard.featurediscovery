namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk452Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk452Discovery() : base(
                                                    "net452-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.5.2",
                                                    "System.dll")

        {
        }
    }
}