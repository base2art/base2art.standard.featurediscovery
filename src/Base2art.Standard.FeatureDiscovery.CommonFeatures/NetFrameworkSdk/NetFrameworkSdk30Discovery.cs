namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System.IO;
    using System.Threading.Tasks;

    public class NetFrameworkSdk30Discovery : IFeatureDiscovery
    {
        public string FeatureName { get; } = "net30-sdk";

        public Task<bool> HasFeature()
        {
            var result = File.Exists(MSBuild.Version12())
                         || File.Exists(MSBuild.Version14())
                         || File.Exists(MSBuild.Version15())
                         || File.Exists(MSBuild.Version16());

            return Task.FromResult(result);
        }
    }
}