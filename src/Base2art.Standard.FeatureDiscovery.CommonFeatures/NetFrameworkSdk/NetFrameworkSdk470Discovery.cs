namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk470Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk470Discovery() : base(
                                                    "net47-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.7",
                                                    "System.dll")

        {
        }
    }
}