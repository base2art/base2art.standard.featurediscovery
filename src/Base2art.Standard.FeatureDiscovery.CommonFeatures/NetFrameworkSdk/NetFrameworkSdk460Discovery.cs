namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk460Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk460Discovery() : base(
                                                    "net46-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.6",
                                                    "System.dll")

        {
        }
    }
}