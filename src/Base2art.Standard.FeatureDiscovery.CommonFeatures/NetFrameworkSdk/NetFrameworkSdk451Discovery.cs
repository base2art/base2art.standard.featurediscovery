namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk451Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk451Discovery() : base(
                                                    "net451-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.5.1",
                                                    "System.dll")

        {
        }
    }
}