namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;
    using System.IO;

    public static class MSBuild
    {
        public static string Version12() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "MSBuild", "12.0",
                                                         "bin", "MSBuild.exe");

        public static string Version14() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "MSBuild", "14.0",
                                                         "bin", "MSBuild.exe");

        public static string Version15() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
                                                         "Microsoft Visual Studio", "2017", "BuildTools", "MSBuild", "15.0", "Bin", "MSBuild.exe");

        public static string Version16() => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
                                                         "Microsoft Visual Studio", "2019", "Enterprise", "MSBuild", "Current", "Bin", "MSBuild.exe");
    }
}