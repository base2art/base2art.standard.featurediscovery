namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk472Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk472Discovery() : base(
                                                    "net472-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.7.2",
                                                    "System.dll")

        {
        }
    }
}