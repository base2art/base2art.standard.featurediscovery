namespace Base2art.FeatureDiscovery.CommonFeatures
{
    using System;

    public class NetFrameworkSdk471Discovery : FileExistsDiscovery
    {
        public NetFrameworkSdk471Discovery() : base(
                                                    "net471-sdk",
                                                    Environment.SpecialFolder.ProgramFilesX86,
                                                    "Reference Assemblies",
                                                    "Microsoft",
                                                    "Framework",
                                                    ".NETFramework",
                                                    "v4.7.1",
                                                    "System.dll")

        {
        }
    }
}